#!/bin/bash

mountpoint='/cache'
lowwatermark=80
highwatermark=90
maxdays=200
mindays=10
d='d'

i=$maxdays
current=$(df $mountpoint | awk '{print $4}' | tail -1 | sed -e 's/[%]//g')
if [ "$current" -lt "$highwatermark" ]; then exit; fi;
while [ "$i" -gt "$mindays" ]; do
current=$(df $mountpoint | awk '{print $4}' | tail -1 | sed -e 's/[%]//g')
if [ "$current" -lt "$lowwatermark" ]; then echo "usage has dropped below $lowwatermark, exiting"; break; fi;
if [ "$current" -gt "$lowwatermark" ]; then echo "usage exceeds $lowwatermark percent, deleting files from $mountpoint older than $i days old"; tmpwatch $i$d $mountpoint; fi;
let i-=1
done
current=$(df $mountpoint | awk '{print $4}' | tail -1 | sed -e 's/[%]//g')
if [ "$current" -gt "$lowwatermark" ]; then echo "couldn't achieve $lowwatermark percent usage by deleting files from $mountpoint older than $mindays days old!"; fi;
